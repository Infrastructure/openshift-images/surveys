FROM docker.io/martialblog/limesurvey:5-fpm

USER 0
RUN apt-get update && apt-get install -y nginx-light && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

ADD nginx.conf /etc/nginx/nginx.conf
ADD entrypoint.sh /entrypoint.sh

RUN chown -R root:root /var/www/html/application/config

USER 1001
ENTRYPOINT ["/entrypoint.sh"]
