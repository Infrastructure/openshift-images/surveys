worker_processes  1;

error_log  /dev/stderr warn;
pid        /tmp/nginx.pid;

events {
    worker_connections  1024;
}

http {
    proxy_temp_path /tmp/proxy_temp;
    client_body_temp_path /tmp/client_temp;
    fastcgi_temp_path /tmp/fastcgi_temp;
    uwsgi_temp_path /tmp/uwsgi_temp;
    scgi_temp_path /tmp/scgi_temp;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    access_log  /dev/stdout  main;

    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    upstream php-fpm {
        server localhost:9000;
    }

    server {
        listen 8080 default_server;
        listen [::]:8080 default_server;

        absolute_redirect off;

        root /var/www/html;
        index index.php;

        client_max_body_size 100M;

        location ~ ^/(application|protected|framework|themes/\w+/views) {
            deny  all;
        }

        location ~ ^/upload/surveys/.*/fu_[a-z0-9]*$ {
            return 444;
        }

        location ~* /upload/.*\.(pl|cgi|py|pyc|pyo|phtml|sh|lua|php|php3|php4|php5|php6|pcgi|pcgi3|pcgi4|pcgi5|pcgi6|icn)$ {
            return 444;
        }

        location = /favicon.ico {
            log_not_found off;
            access_log off;
        }

        location = /robots.txt {
            allow all;
            log_not_found off;
            access_log off;
        }

        location / {
            try_files $uri /index.php?$args;
        }

        location /assets {
            alias /var/www/html/assets;
        }

        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_intercept_errors on;
            fastcgi_read_timeout 300;
            fastcgi_param  HTTPS 'on';
            fastcgi_pass php-fpm;
            fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }

        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
            expires max;
            log_not_found off;
        }

        location /nginx_status {
            return 200;
        }
    }
}
